#!/bin/bash
#PBS -q express
#PBS -P ku36
#PBS -l ncpus=48
#PBS -l mem=48GB
#PBS -l walltime=08:00:00
#PBS -j oe
#PBS -M marco.kulueke@murdoch.edu.au
#PBS -m ae

source /etc/profile.d/modules.sh
module load ncl/6.6.2

ncl /home/Marco/scripts_gadi/postprocessing/calc_means.ncl
