;*********************************************************************************************************************
;* calculates:
;*	- total precipitation
;*	- monthly daily temperature maximum and minimun mean
;*	- monthly SST
;*********************************************************************************************************************
begin

;*************
; Domain
  dom = "02"; specify domain here: "01" or "02"
;************

  experiment_list = (/"run_WRF_control_YSU_KF", "run_WRF_ens_mem_1", "run_WRF_ens_mem_2", "run_WRF_ens_mem_3", "run_WRF_ens_mem_4", "run_WRF_experiment_YSU_KF"/); specify experiment(s)
  file_path = "/data2/Marco_Ningaloo_Nino/WRFV402/WRF/WRFV3/run/"
  path_out = "/home/Marco/gadi_data/wrf_output_means/"
  wrf_date = (/"2010-12", "2011-01", "2011-02", "2011-03", "2011-04"/); specify months

; define lenght of each month (no leap year)
  days_per_month = (/31., 28., 31., 30., 31., 30., 31., 31., 30., 31., 30., 31./)
  days_per_month!0 = "month"
  days_per_month&month = fspan(1, 12, 12)  

  do exp_no = 0, dimsizes(experiment_list) -1
    experiment = experiment_list(exp_no)
  
  ; define output array (only necessary once)  
    f = addfile(file_path +experiment +"/wrfout_d" +dom +"_" +wrf_date(0) +"-01_00:00:00", "r")
    
    t = f->T2
  
    dimsizes_t = dimsizes(t)
    t_dmax_avg = new((/12, dimsizes_t(1), dimsizes_t(2)/), typeof(t))
    t_dmax_avg!0 = "month"
    t_dmax_avg&month = fspan(1, 12, 12)
  
  ; define other variables  
    t_dmin_avg = t_dmax_avg
    sst_d_avg = t_dmax_avg; daily SST average
   
    delete([/f, t/])
    
  ; loop over each month
    do j = 0, dimsizes(wrf_date) -1
  
      wrf_date_char = stringtochar(wrf_date(j))
      year = stringtofloat(charactertostring(wrf_date_char(0:3)))
      month = stringtofloat(charactertostring(wrf_date_char(5:6)))
  
      print("year: " +year +", month: " +month)
  
      f = addfile(file_path +experiment +"/wrfout_d" +dom +"_" +wrf_date(j) +"-01_00:00:00", "r")
      
      print("day: 01")
  
  ; read first day to define month_avg array and read first precipitation
      t = f->T2
      t_dmax_avg_month = dim_max_n_Wrap(t, (/0/)) 
      t_dmin_avg_month = dim_min_n_Wrap(t, (/0/))
  
      sst = f->SST
      sst_d_avg_month = dim_avg_n_Wrap(sst, (/0/))
    
      delete([/f, t, sst/])
    
   ; loop over all days starting from second day
      do i = 2, days_per_month({month})
  
        if i .lt. 10 then
          day = "0" +tostring(i)
        else
          day = tostring(i)
        end if
  
        print("day: " +day)
  
        f = addfile(file_path +experiment +"/wrfout_d" +dom +"_" +wrf_date(j) +"-" +day +"_00:00:00", "r")
        t = f->T2
        t_dmax = dim_max_n_Wrap(t, (/0/))
        t_dmin = dim_min_n_Wrap(t, (/0/))
        t_dmax_avg_month = t_dmax_avg_month +t_dmax
        t_dmin_avg_month = t_dmin_avg_month +t_dmin
  
        sst = f->SST
        sst_d = dim_avg_n_Wrap(sst, (/0/))
        sst_d_avg_month = sst_d_avg_month +sst_d
   
        delete([/f, t, t_dmax, t_dmin, sst, sst_d/])
      end do
  
  ; write results to output array    
      t_dmax_avg({month}, :, :) = t_dmax_avg_month/days_per_month({month})
      t_dmin_avg({month}, :, :) = t_dmin_avg_month/days_per_month({month})
  
      sst_d_avg({month}, :, :) = sst_d_avg_month/days_per_month({month})

        delete([/t_dmax_avg_month, t_dmin_avg_month, sst_d_avg_month, month/])
  
    end do
  
  ; write to file
    fo = addfile(path_out + experiment +"_d" +dom +"_full_period_sst_t.nc", "c"); change name if necessary
  
    fo->t_max = t_dmax_avg
    fo->t_min = t_dmin_avg
  
    fo->sst_avg = sst_d_avg
  
    delete([/fo, t_dmax_avg, t_dmin_avg, sst_d_avg/])
  end do
end
