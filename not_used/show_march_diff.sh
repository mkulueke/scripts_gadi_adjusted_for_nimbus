#!/bin/bash

source /etc/profile.d/modules.sh
module load cdo/1.9.8

/scratch/ku36/mk5729/WPS_out/lin_int_ens_mem/show_march_diff

count=-5

cdo -timavg -selvar,SST ../ens_mem_0/wrflowinp_d01_2011_03 ctrl_sst_avg_d01_2011_03
cdo -timavg -selvar,SST ../ens_mem_0/wrflowinp_d02_2011_03 ctrl_sst_avg_d02_2011_03

while [ $count -le 10 ]; do
  echo $count
  
  if [ -d "tmp_d0*" ]; then rm -Rf tmp_d0*; fi  

  cdo -timavg -selvar,SST ../ens_mem_${count}/wrflowinp_d01_2011_03 tmp_d01
  cdo -sub tmp_d01 ctrl_sst_avg_d01_2011_03 ens_mem_${count}_exp-ctrl_sst_avg_d01_2011_03

  cdo -timavg -selvar,SST ../ens_mem_${count}/wrflowinp_d02_2011_03 tmp_d02
  cdo -sub tmp_d02 ctrl_sst_avg_d02_2011_03 ens_mem_${count}_exp-ctrl_sst_avg_d02_2011_03

  count=$(($count+1))
done
