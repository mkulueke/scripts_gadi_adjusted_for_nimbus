#!/bin/csh -f
#PBS -q express
#PBS -P ku36
#PBS -l ncpus=1
#PBS -l mem=6GB
#PBS -l walltime=03:00:00
#PBS -j oe
#PBS -M marco.kulueke@murdoch.edu.au
#PBS -m ae

source /etc/profile.d/modules.csh
module load ncl/6.6.2

cd /scratch/ku36/mk5729/WRFV402/WRF/WRFV3/run/run_WRF_control_YSU_KF/wrfout_march

# list the files in the directory, loop through each file
foreach file_pre (`ls wrfout_d* | sed 's/\:00\:00//g'`)
  # set the file_in and file_out values
  set file_in = {$file_pre}:00:00
  echo {$file_in}
  set file_out = {$file_pre}-cf.nc
  # run the wrfout_to_cf NCL script
  ncl 'file_in="'{$file_in}'"' 'file_out="'{$file_out}'"' wrfout_to_cf.ncl
end
