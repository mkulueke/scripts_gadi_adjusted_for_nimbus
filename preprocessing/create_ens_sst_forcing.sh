#!/bin/bash
#PBS -q express
#PBS -P ku36
#PBS -l ncpus=1
#PBS -l mem=6GB
#PBS -l walltime=03:00:00
#PBS -j oe
#PBS -M marco.kulueke@murdoch.edu.au
#PBS -m ae

source /etc/profile.d/modules.sh
module load cdo/1.9.8
module load ncl/6.6.2

ens_no=$(expr 15)
months=(2010_10 2010_11 2010_12 2011_01 2011_02 2011_03 2011_04 2011_05)
doms=(d01 d02)

cd /data2/Marco_Ningaloo_Nino

#if [ -d "tmp" ]; then rm -Rf tmp; fi
#
#mkdir tmp
#
#for month in ${months[@]}; do
#  for dom in ${doms[@]}; do
#    echo Calc difference for ${month}
#    cdo sub ../experiment_YSU_KF/wrflowinp_${dom}_${month} ../control_YSU_KF/wrflowinp_${dom}_${month} tmp/exp-ctrl_wrflowinp_${dom}_${month}
#  done
#done

count=$(expr -${ens_no} / 3)
while [ $count -le 10 ]; do
  factor=$(echo "scale=2;3*$count/${ens_no}" | bc -l)
  echo ${factor}
  if [ -d "ens_mem_${count}" ]; then rm -Rf ens_mem_${count}; fi
  mkdir ens_mem_${count}
  mkdir ens_mem_${count}/wrong_format

  cd ens_mem_${count}
  ln -s ../../control_YSU_KF/wrfbdy* .
  ln -s ../../control_YSU_KF/wrfinput* .
  ln -s ../../control_YSU_KF/wrffdda* .
  cp ../../control_YSU_KF/wrflowinp* .
  cd ..

  for month in ${months[@]}; do
    for dom in ${doms[@]}; do
      echo Working on:
      echo Ensemble member $count
      echo wrflowinp_${dom}_${month}
      cdo -mulc,${factor} tmp/exp-ctrl_wrflowinp_${dom}_${month} tmp_1
      cdo -add ../control_YSU_KF/wrflowinp_${dom}_${month} tmp_1 ens_mem_${count}/wrong_format/${dom}_${month} 
      rm tmp_1 tmp_2
      ncl 'original="/scratch/ku36/mk5729/WPS_out/lin_int_ens_mem/ens_mem_'${count}'/wrflowinp_'${dom}'_'${month}'"' 'experiment="/scratch/ku36/mk5729/WPS_out/lin_int_ens_mem/ens_mem_'${count}'/wrong_format/'${dom}'_'${month}'"' /home/585/mk5729/scripts_gadi/replace_sst.ncl
    done
  done
  rm -r sst

  count=`expr $count + 1`
done
