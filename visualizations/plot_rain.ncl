load "./cordex_grid_plot_functions_modified.ncl"

begin
f_lat_lon = addfile("/data2/Marco_Ningaloo_Nino/WPS_out/control_YSU_KF/wrfinput_d01_2011_05","r")
lat = f_lat_lon->XLAT(0,:,:)
lon = f_lat_lon->XLONG(0,:,:)

f_plot = addfile("/home/Marco/gadi_data/wrf_output_means/run_WRF_control_YSU_KF_d01.nc","r")
res = True
col_map = "precip3_16lev"
type = "png"
include_wheatbelt = 0


var_plot = f_plot->rain(2,:,:); select March
tiMainString = ""

file_save = "abs_rain_d01"
cnMinLevelValF = 0.;min(fi)
cnMaxLevelValF = 400.;max(fi)
cnLevelSpacingF = 400./20.
gsnPanelMainString = ""
lbTitleString = ""

plot = plot_cordex_single(f_lat_lon,res,col_map,type,file_save,var_plot,lat,lon,tiMainString,lbTitleString,include_wheatbelt,cnMinLevelValF,cnMaxLevelValF,cnLevelSpacingF)


end
