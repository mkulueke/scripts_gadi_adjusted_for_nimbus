load "./cordex_grid_plot_functions_modified.ncl"

begin
f_lat_lon = addfile("/data2/Marco_Ningaloo_Nino/WPS_out/control_YSU_KF/wrfinput_d01_2011_05","r")
lat = f_lat_lon->XLAT(0,:,:)
lon = f_lat_lon->XLONG(0,:,:)

f_plot = addfile("/home/Marco/gadi_data/wrf_output_means/run_WRF_control_YSU_KF_d01.nc","r")
res = True
col_map = "matlab_jet"
type = "pdf"
include_wheatbelt = 0


var_plot = f_plot->slp(2,:,:); select March
tiMainString = ""

file_save = "abs_slp_d01"
cnMinLevelValF = 1005.;min(fi)
cnMaxLevelValF = 1025.;max(fi)
cnLevelSpacingF = 20./10.
gsnPanelMainString = ""
lbTitleString = "hPa"

relax_points = 10
var_plot_cropped = remove_boundary_points(var_plot, relax_points)
lat_cropped = remove_boundary_points(lat, relax_points)
lon_cropped = remove_boundary_points(lon, relax_points)

plot = plot_cordex_single(f_lat_lon,res,col_map,type,file_save,var_plot_cropped,lat_cropped,lon_cropped,tiMainString,lbTitleString,include_wheatbelt,cnMinLevelValF,cnMaxLevelValF,cnLevelSpacingF)


end
